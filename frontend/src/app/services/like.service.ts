import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) {}

    public likePost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike,
            userId: currentUser.id,
        };

        // update current array instantly
        let hasReaction: Reaction = innerPost.reactions.find(
            (x) => x.user.id === currentUser.id
        );
        const changeReaction: boolean =
            hasReaction !== undefined
                ? hasReaction.isLike !== reaction.isLike
                : false;

        innerPost.reactions =
            hasReaction !== undefined
                ? changeReaction
                    ? innerPost.reactions
                        .filter((x) => x.user.id !== currentUser.id)
                        .concat({ isLike, user: currentUser })
                    : innerPost.reactions.filter(
                        (x) => x.user.id !== currentUser.id
                    )
                : innerPost.reactions.concat({
                    isLike,
                    user: currentUser,
                });

        hasReaction = innerPost.reactions.find(
            (x) => x.user.id === currentUser.id
        );

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions =
                    hasReaction !== undefined
                        ? changeReaction
                            ? innerPost.reactions
                                .filter((x) => x.user.id !== currentUser.id)
                                .concat({ isLike, user: currentUser })
                            : innerPost.reactions.filter(
                                (x) => x.user.id !== currentUser.id
                            )
                        : innerPost.reactions.concat({
                            isLike,
                            user: currentUser,
                        });

                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, currentUser: User, isLike: boolean) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike,
            userId: currentUser.id,
        };

        // update current array instantly
        let hasReaction: Reaction = innerComment.reactions.find(
            (x) => x.user.id === currentUser.id
        );
        const changeReaction: boolean =
            hasReaction !== undefined
                ? hasReaction.isLike !== reaction.isLike
                : false;

        innerComment.reactions =
            hasReaction !== undefined
                ? changeReaction
                    ? innerComment.reactions
                        .filter((x) => x.user.id !== currentUser.id)
                        .concat({ isLike, user: currentUser })
                    : innerComment.reactions.filter(
                        (x) => x.user.id !== currentUser.id
                    )
                : innerComment.reactions.concat({
                    isLike,
                    user: currentUser,
                });

        hasReaction = innerComment.reactions.find(
            (x) => x.user.id === currentUser.id
        );

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions =
                    hasReaction !== undefined
                        ? changeReaction
                            ? innerComment.reactions
                                .filter((x) => x.user.id !== currentUser.id)
                                .concat({ isLike, user: currentUser })
                            : innerComment.reactions.filter(
                                (x) => x.user.id !== currentUser.id
                            )
                        : innerComment.reactions.concat({
                            isLike,
                            user: currentUser,
                        });

                return of(innerComment);
            })
        );
    }
}
