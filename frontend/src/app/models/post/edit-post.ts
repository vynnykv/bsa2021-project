export interface EditPost {
    id: number;
    authorId: number;
    previewImage: string;
    body: string;
}
