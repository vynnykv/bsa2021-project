import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, pipe, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { EditPost } from '../../models/post/edit-post';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { PostService } from '../../services/post.service';
import { GyazoService } from '../../services/gyazo.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy, OnInit {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Output() onDelete = new EventEmitter<number>();

    public showComments = false;
    public newComment = {} as NewComment;

    public imageFile: File;
    public postDetail !: FormGroup;
    public postObj = {} as EditPost;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private formBuilder: FormBuilder,
        private gyazoService: GyazoService
    ) {}

    ngOnInit(): void {
        this.postDetail = this.formBuilder.group({
            id: [''],
            authorId: [''],
            previewImage: [''],
            body: ['']
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public postReaction(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public editPost(post: Post) {
        this.postDetail.controls.id.setValue(post.id);
        this.postDetail.controls.authorId.setValue(post.author.id);
        this.postDetail.controls.body.setValue(post.body);
        this.postDetail.controls.previewImage.setValue(post.previewImage);
    }

    public updatePost() {
        this.postObj.id = this.postDetail.value.id;
        this.postObj.authorId = this.postDetail.value.authorId;
        this.postObj.body = this.postDetail.value.body;
        this.postObj.previewImage = this.postDetail.value.previewImage;
        const postSubscription = !this.imageFile
            ? this.postService.editPost(this.postObj)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.postObj.previewImage = imageData.url;
                    return this.postService.editPost(this.postObj);
                })
            );
        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.post.body = this.postObj.body;
                this.post.previewImage = this.postObj.previewImage;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public deletePost(id: number) {
        this.postService
            .deletePost(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
                this.onDelete.emit(id);
            });
    }

    public updateComments(id: number) {
        this.post.comments = this.post.comments.filter(c => c.id !== id);
    }

    public GetLikes(): number {
        return this.post.reactions.filter((reaction) => reaction.isLike).length;
    }

    public GetDislikes(): number {
        return this.post.reactions.length - this.GetLikes();
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        console.log(reader.result as string);
        reader.readAsDataURL(this.imageFile);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

}
