import { Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { CommentService } from '../../services/comment.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UpdateComment } from '../../models/comment/update-comment';
import {catchError, switchMap, takeUntil} from 'rxjs/operators';
import {empty, Observable, Subject} from 'rxjs';
import { SnackBarService } from '../../services/snack-bar.service';
import {DialogType} from '../../models/common/auth-dialog-type';
import {AuthenticationService} from '../../services/auth.service';
import {AuthDialogService} from '../../services/auth-dialog.service';
import {LikeService} from '../../services/like.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit, OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() onDelete = new EventEmitter<number>();

    public commentDetail !: FormGroup;
    public commentObj = {} as UpdateComment;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private formBuilder: FormBuilder,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService) {}

    ngOnInit() {
        this.commentDetail = this.formBuilder.group({
            id: [''],
            body: ['']
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public editComment(comment: Comment) {
        this.commentDetail.controls.id.setValue(comment.id);
        this.commentDetail.controls.body.setValue(comment.body);
    }

    public updateComment() {
        this.commentObj.id = this.commentDetail.value.id;
        this.commentObj.body = this.commentDetail.value.body;

        this.commentService.updateComment(this.commentObj)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((res) => this.comment.body = this.commentObj.body,
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deleteComment(id: number) {
        this.commentService
            .deleteComment(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
                this.onDelete.emit(id);
            });
    }

    public commentReaction(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public GetLikes(): number {
        return this.comment.reactions.filter((reaction) => reaction.isLike).length;
    }

    public GetDislikes(): number {
        return this.comment.reactions.length - this.GetLikes();
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
