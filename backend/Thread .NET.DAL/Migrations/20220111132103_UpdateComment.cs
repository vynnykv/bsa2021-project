﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class UpdateComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(6129), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(7229), 12 },
                    { 18, 9, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9088), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9097), 15 },
                    { 17, 15, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9040), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9048), 4 },
                    { 15, 11, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8942), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8950), 13 },
                    { 14, 7, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8895), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8902), 14 },
                    { 13, 17, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8845), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8852), 19 },
                    { 12, 20, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8799), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8807), 12 },
                    { 11, 20, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8754), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8762), 13 },
                    { 19, 7, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9136), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9144), 20 },
                    { 10, 16, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8706), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8715), 2 },
                    { 8, 17, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8608), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8617), 4 },
                    { 7, 20, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8555), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8564), 2 },
                    { 6, 11, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8496), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8508), 8 },
                    { 5, 11, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8314), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8322), 20 },
                    { 4, 7, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8263), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8272), 19 },
                    { 3, 18, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8212), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8220), 7 },
                    { 2, 14, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8127), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8144), 20 },
                    { 9, 17, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8657), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8666), 13 },
                    { 20, 8, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9183), false, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(9191), 15 },
                    { 16, 19, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8992), true, new DateTime(2022, 1, 11, 15, 21, 2, 445, DateTimeKind.Local).AddTicks(8999), 3 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Aut maiores tempore ab nihil libero fugit ut.", new DateTime(2022, 1, 11, 15, 21, 2, 425, DateTimeKind.Local).AddTicks(8427), 5, new DateTime(2022, 1, 11, 15, 21, 2, 425, DateTimeKind.Local).AddTicks(9803) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Qui ullam ut incidunt temporibus sed.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(1854), 12, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(1893) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Odit aliquid et alias eum ea.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2090), 2, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2102) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Aut laboriosam rerum quasi ipsum nihil veniam nam nemo.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2386), 2, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2399) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 16, "Quia delectus dolor voluptas autem delectus omnis.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2532), new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2545) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Quaerat rerum ut laborum provident consequatur tempore.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2747), 2, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2759) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Sit molestias quo rerum similique distinctio magni perspiciatis.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2891), 3, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(2901) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Reprehenderit dolores saepe eveniet iure odio tempore consequatur reprehenderit.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3034), 7, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3045) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Eius beatae earum non iste saepe repudiandae.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3244), 13, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3257) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Ducimus non cumque magnam.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3355), 16, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3364) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Quo aut consequatur quia omnis accusamus qui facilis.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3477), 3, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3487) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Alias expedita cum amet soluta repellat dolores aut et magni.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3616), 14, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3626) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Magni dolorem quaerat.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3722), 8, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3732) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Dolores ratione voluptates nostrum.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3824), 10, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(3834) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Ullam odio non corrupti aut voluptas placeat.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4017), 20, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4030) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Blanditiis quaerat nulla cumque possimus corrupti magni qui voluptatem ea.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4161), 2, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4171) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Laborum ea veniam sint aut dolor sunt.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4285), 3, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4296) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Sequi optio illo mollitia eos dolore qui ut.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4422), 3, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4433) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Et temporibus occaecati et.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4532), 14, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4544) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Nihil nostrum aspernatur possimus modi eum animi dicta animi.", new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4746), 15, new DateTime(2022, 1, 11, 15, 21, 2, 426, DateTimeKind.Local).AddTicks(4758) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 962, DateTimeKind.Local).AddTicks(2943), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/876.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(233) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1271), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/809.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1289) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1346), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/860.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1355) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1396), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/73.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1404) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1444), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/549.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1452) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1492), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1219.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1501) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1539), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1248.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1548) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1586), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1225.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1594) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1630), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/78.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1674), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/403.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1683) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1720), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/269.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1729) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1766), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/49.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1774) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1812), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/536.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1819) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1854), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1132.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1862) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1900), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/729.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1908) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(1947), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/343.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2045) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2090), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/626.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2098) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2131), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/919.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2139) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2173), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/70.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2182) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2216), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/119.jpg", new DateTime(2022, 1, 11, 15, 21, 1, 963, DateTimeKind.Local).AddTicks(2224) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(503), "https://picsum.photos/640/480/?image=278", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(1598) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2022), "https://picsum.photos/640/480/?image=567", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2079), "https://picsum.photos/640/480/?image=327", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2088) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2125), "https://picsum.photos/640/480/?image=188", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2134) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2273), "https://picsum.photos/640/480/?image=545", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2284) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2324), "https://picsum.photos/640/480/?image=630", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2332) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2369), "https://picsum.photos/640/480/?image=825", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2377) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2415), "https://picsum.photos/640/480/?image=737", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2424) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2461), "https://picsum.photos/640/480/?image=445", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2470) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2507), "https://picsum.photos/640/480/?image=38", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2515) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2549), "https://picsum.photos/640/480/?image=489", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2557) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2591), "https://picsum.photos/640/480/?image=489", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2599) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2635), "https://picsum.photos/640/480/?image=40", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2642) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2678), "https://picsum.photos/640/480/?image=425", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2687) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2722), "https://picsum.photos/640/480/?image=220", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2730) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2767), "https://picsum.photos/640/480/?image=632", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2776) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2810), "https://picsum.photos/640/480/?image=717", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2817) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2928), "https://picsum.photos/640/480/?image=441", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2937) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2979), "https://picsum.photos/640/480/?image=1051", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(2987) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(3023), "https://picsum.photos/640/480/?image=675", new DateTime(2022, 1, 11, 15, 21, 1, 971, DateTimeKind.Local).AddTicks(3032) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3839), false, 18, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3848), 11 },
                    { 1, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(954), true, 13, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2010), 19 },
                    { 19, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3788), true, 12, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3797), 2 },
                    { 18, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3737), true, 7, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3746), 18 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3685), true, 16, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3695), 9 },
                    { 16, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3631), false, 1, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3640), 14 },
                    { 14, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3486), false, 14, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3495), 11 },
                    { 13, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3437), false, 6, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3446), 10 },
                    { 12, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3387), true, 5, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3395), 18 },
                    { 11, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3337), true, 9, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3346), 14 },
                    { 15, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3534), false, 8, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3541), 17 },
                    { 9, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3237), false, 8, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3245), 14 },
                    { 8, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3184), true, 14, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3195), 7 },
                    { 7, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3130), false, 13, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3138), 1 },
                    { 6, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3078), true, 13, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3085), 17 },
                    { 10, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3288), true, 20, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3297), 5 },
                    { 5, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3029), true, 19, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(3037), 16 },
                    { 4, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2980), false, 11, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2987), 8 },
                    { 3, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2930), true, 18, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2939), 15 },
                    { 2, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2859), true, 14, new DateTime(2022, 1, 11, 15, 21, 2, 436, DateTimeKind.Local).AddTicks(2872), 12 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Nihil rerum quam distinctio beatae rerum libero.\nSimilique quam et temporibus voluptatum ut in sequi.\nEum blanditiis quos.\nDeserunt magnam et deserunt ea temporibus et repellat.\nEa et natus perspiciatis ab omnis.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(2885), 37, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(4005) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "ut", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(5583), 23, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(5611) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Placeat molestiae et sed corrupti corrupti qui commodi architecto sit.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7046), 38, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7075) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Et cum nesciunt aut est quia veritatis optio incidunt non.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7325), 39, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7339) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Tempore corrupti aliquid occaecati voluptatem.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7483), 28, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7502) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "id", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7637), 34, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(7647) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Qui autem tempore recusandae neque pariatur quidem atque vel suscipit.\nAspernatur eum illo velit accusamus sed.\nIpsum aut est officia fugiat ut.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(8350), 36, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(8374) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Architecto corrupti molestiae dolor molestiae et ut at aliquam voluptatibus.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(8568), 38, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(8581) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Perferendis dicta modi provident fuga ut minus laudantium.\nNisi dignissimos nemo eos perspiciatis quia eos magni ut id.", new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(8906), 29, new DateTime(2022, 1, 11, 15, 21, 2, 413, DateTimeKind.Local).AddTicks(8921) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Omnis vitae nisi laborum. Id cumque aut et. Illum voluptatem sunt dolores soluta non laboriosam voluptatem sint fugiat. Omnis eum nemo non voluptas minus.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(1018), 39, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(1057) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Eum ab est. Fuga sed et enim facilis nisi aut. Est veritatis similique harum. Aut quasi corporis reprehenderit quam eligendi alias magnam aperiam.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(1496), 34, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(1512) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Quisquam similique impedit et. Est eos corporis eius omnis quia consectetur. Tenetur id culpa et. Aspernatur architecto nesciunt impedit quae. Voluptate ex assumenda nostrum deserunt iusto et iusto. Quia vitae expedita sit harum ipsa voluptates porro et at.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(4100), 31, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(4134) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "natus", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(4330), 21, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(4342) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Aut est eligendi reiciendis perferendis occaecati asperiores voluptatem dolorem.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(4529), 40, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(4543) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Assumenda ipsam non officiis hic error ut incidunt amet ea.\nVero cupiditate provident dolor quibusdam quisquam facilis eos.\nOmnis nam amet tempora et dolores voluptatem.\nOfficiis tenetur accusamus repellendus quasi est esse.\nEt quia optio necessitatibus culpa accusamus officia sit voluptatem nesciunt.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(5189), 40, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(5207) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Sed in dolorem sequi. Magni nihil aut. Perspiciatis fuga ea ut corrupti officiis repellendus repudiandae fugit. Ipsum unde hic magnam eaque.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(5717), 38, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(5737) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Eos natus assumenda quas porro libero quia et blanditiis enim. Molestiae consequatur rerum ut a excepturi ex voluptas ducimus. Dicta ut sunt sed est. Voluptas consequuntur voluptatem doloremque minima. Saepe tempore voluptatibus vero at sint qui nam aut. Aut corrupti dolore dolor veritatis ut.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6314), 22, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Ut ab non adipisci corporis doloremque.\nAut omnis consectetur minima et consequuntur.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6529), 30, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Expedita quo voluptatem.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6639), 36, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6650) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "A excepturi consectetur.\nQuae necessitatibus nemo enim laboriosam ex.\nAssumenda distinctio omnis atque ea repellendus odit.\nCupiditate doloribus sed nihil pariatur.", new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(6989), 31, new DateTime(2022, 1, 11, 15, 21, 2, 414, DateTimeKind.Local).AddTicks(7003) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2022, 1, 11, 15, 21, 2, 46, DateTimeKind.Local).AddTicks(7938), "Theresa23@yahoo.com", "5KCzswcTamx4a8ozProkj0Cch5gh9/GVA4eQ5mntLS0=", "rQbe2majC/VCui29vopTOJkg34nk6eNQKdKBYx0qHC4=", new DateTime(2022, 1, 11, 15, 21, 2, 46, DateTimeKind.Local).AddTicks(9249), "Frederic_Mueller36" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2022, 1, 11, 15, 21, 2, 61, DateTimeKind.Local).AddTicks(6970), "Rahul_Armstrong@hotmail.com", "v33msBU/ASrN5yynfaNekv33KARE178Y/lMMWCU7Xuk=", "/BO0RgPsbYpdytwcosIkZr0+MEJj/uM5Yad2yzJCWOo=", new DateTime(2022, 1, 11, 15, 21, 2, 61, DateTimeKind.Local).AddTicks(7066), "Obie_Kerluke62" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2022, 1, 11, 15, 21, 2, 76, DateTimeKind.Local).AddTicks(9870), "Cornelius_Quigley@gmail.com", "8fye6haTPh3X67GMc6gzFzsGgIMmHF/CFleU1KzSHns=", "3I135wI/jfs/XE3+Xunv9faCrhQJN09N8xonk5+mWME=", new DateTime(2022, 1, 11, 15, 21, 2, 76, DateTimeKind.Local).AddTicks(9955), "Shea61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2022, 1, 11, 15, 21, 2, 94, DateTimeKind.Local).AddTicks(754), "Cordell_Lemke21@hotmail.com", "nvKDwHMG72pfbmOxr7HY+7PEG2ZJCgnxqDSwewxgrGo=", "LkzTVTJ5KQ+6RP7L9cgsiiqFvE/1lZ4gD9sUUi3hiDo=", new DateTime(2022, 1, 11, 15, 21, 2, 94, DateTimeKind.Local).AddTicks(834), "Madisyn_Bailey75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2022, 1, 11, 15, 21, 2, 114, DateTimeKind.Local).AddTicks(5665), "Calista_Wilderman@yahoo.com", "xuj73uzHgYwT7NSCCOIIMDMWrhX5QxiK0/Zik31ihuE=", "Tc+HfsNtu8W2QtUGzQhyRl1aDXSR2BemBNWaTU3pMGo=", new DateTime(2022, 1, 11, 15, 21, 2, 114, DateTimeKind.Local).AddTicks(5735), "Cade_Hammes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2022, 1, 11, 15, 21, 2, 125, DateTimeKind.Local).AddTicks(2838), "Cecilia21@hotmail.com", "ta60OacO2mruIST6jaqLPdZyXwxg7v6qf333N4ydbPU=", "3O8fuz8S2tNykBKTkkaTc2LfVjlGga5BnsV+TWuZutg=", new DateTime(2022, 1, 11, 15, 21, 2, 125, DateTimeKind.Local).AddTicks(2919), "Alejandrin.Waters50" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2022, 1, 11, 15, 21, 2, 137, DateTimeKind.Local).AddTicks(2836), "Fern1@gmail.com", "2N9M4YNF7Pg0rkFgFR6wNweIiKX/HE/+xrw5MdIjYUg=", "Jd3N8dew0cPD81kLe451+evCOiZGLZP+Pa6NP3CzlDQ=", new DateTime(2022, 1, 11, 15, 21, 2, 137, DateTimeKind.Local).AddTicks(2919), "Ruby52" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2022, 1, 11, 15, 21, 2, 150, DateTimeKind.Local).AddTicks(4815), "Art_Halvorson@hotmail.com", "kYBMlzKa/nh0gGKWH2JJU8Iq3SOOEolJmuUIT3ak7W4=", "aOf5tscPT7mjJG1RaS7oxVlIlyt0DpwdxnVGt+ZTn8k=", new DateTime(2022, 1, 11, 15, 21, 2, 150, DateTimeKind.Local).AddTicks(4889), "Art79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2022, 1, 11, 15, 21, 2, 167, DateTimeKind.Local).AddTicks(6350), "Alden_Bashirian@hotmail.com", "uMSHgcbl+YpTsReELxWEGn5TV3LXOZMEmKEyN1Cd2g0=", "HweTciDVgEiH6Vq6+y7Kwg2fQHCM93kfAivGxeEM1OQ=", new DateTime(2022, 1, 11, 15, 21, 2, 167, DateTimeKind.Local).AddTicks(6432), "Ottis13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2022, 1, 11, 15, 21, 2, 182, DateTimeKind.Local).AddTicks(5356), "River.Stanton@yahoo.com", "4iqWbl6dGT8h9EkGfjggJOqPDD2dk0sNey+Vd710DNA=", "epLAJn77GFtQrBTwbIbyJ1nQ1JdyMlSL89CxAMX8NHg=", new DateTime(2022, 1, 11, 15, 21, 2, 182, DateTimeKind.Local).AddTicks(5439), "Malika.Metz" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2022, 1, 11, 15, 21, 2, 193, DateTimeKind.Local).AddTicks(6700), "Josiane_Weimann98@yahoo.com", "M43eA3dsC+A8TRgLoLqYEYOq6DqAUQStCQ0OFXuO+Ns=", "pK1pqkWw+XK34QGmnDzWZCQoICJdU/EQjFVKmtIPWPs=", new DateTime(2022, 1, 11, 15, 21, 2, 193, DateTimeKind.Local).AddTicks(6781), "Ford.Moore" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2022, 1, 11, 15, 21, 2, 209, DateTimeKind.Local).AddTicks(4902), "Morgan.Wintheiser82@yahoo.com", "pAxGl+0AYuhWBHYsJKURPK5a7wLmz5UgcNBsR4Dy0HE=", "HNxJrA/zOLdJA+JzY4d2R9eekkC1OuVQXy9LwY7/2ng=", new DateTime(2022, 1, 11, 15, 21, 2, 209, DateTimeKind.Local).AddTicks(4990), "Adrien.Cummerata" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2022, 1, 11, 15, 21, 2, 236, DateTimeKind.Local).AddTicks(7179), "Mohammad50@yahoo.com", "X5lLDuksmgcOMGCN0lRhni3lovW9HsNm/5mGfoMVC6s=", "1AcQah/Jtn8UolmHLoDt0KEMajxPDseFIqxhYC7Wnec=", new DateTime(2022, 1, 11, 15, 21, 2, 236, DateTimeKind.Local).AddTicks(7258), "Floy_Larkin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 2, 252, DateTimeKind.Local).AddTicks(4434), "Thalia_Wunsch24@hotmail.com", "U5W8+5pKCPmZIqR4kYWPAqGXHXHZQZ5oV9zSa9halfE=", "wV/HkFxZUmNb4EGXJLLWijcxMwEc5arzqQNABGFR/gk=", new DateTime(2022, 1, 11, 15, 21, 2, 252, DateTimeKind.Local).AddTicks(4510), "Mariano4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2022, 1, 11, 15, 21, 2, 270, DateTimeKind.Local).AddTicks(9437), "Kenyatta_Lang@yahoo.com", "4KdgJr4vW9CVOQY2otvbkl9JWIZMKyqD2ZnCv7DVbzw=", "yonxef1HsKPXvJEfC4xXXjkKf7XNpPL0k8S3bkHzZSg=", new DateTime(2022, 1, 11, 15, 21, 2, 270, DateTimeKind.Local).AddTicks(9540), "Shanon_Toy96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2022, 1, 11, 15, 21, 2, 290, DateTimeKind.Local).AddTicks(7912), "Noemie13@gmail.com", "uB0tP3m2hJgh6xMZQFjtruD/wG9xHqLNZdzpQeqQ/rs=", "yz6Cxj8wnxNOMWxuJ/OI06fe8Bpyn49WU+sDiiCNhRs=", new DateTime(2022, 1, 11, 15, 21, 2, 290, DateTimeKind.Local).AddTicks(8011), "Sharon_Waelchi" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2022, 1, 11, 15, 21, 2, 306, DateTimeKind.Local).AddTicks(2665), "Keith86@hotmail.com", "1qVasgXS/VR5zLtN30brILZX0dtBCt7Ztxqh3gfLfws=", "H1537PDCZ7L+Fr8tHhYiQgAWTqEoI8iWfxUqWYzsPWg=", new DateTime(2022, 1, 11, 15, 21, 2, 306, DateTimeKind.Local).AddTicks(2761), "Randi1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2022, 1, 11, 15, 21, 2, 322, DateTimeKind.Local).AddTicks(5354), "Norbert80@hotmail.com", "rg02wWzJKEmZ6QbxPD3BRSm/jCxe6tu5wSqitF33tYE=", "701eQB35crJDAvsBGSfykOOx/VM95TzqSuOh0qhT+N0=", new DateTime(2022, 1, 11, 15, 21, 2, 322, DateTimeKind.Local).AddTicks(5452), "Rickie48" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2022, 1, 11, 15, 21, 2, 359, DateTimeKind.Local).AddTicks(3864), "Clovis_Bradtke@gmail.com", "6hokvOoHcpM6GlTo4thL2cc5+2I0wHPTeROJ81ikdbo=", "GX7fnpNyqy77nR1i0ZNhsh09kcrmipIyc1sUjjpqYYE=", new DateTime(2022, 1, 11, 15, 21, 2, 359, DateTimeKind.Local).AddTicks(3989), "Laurine_Gerlach96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2022, 1, 11, 15, 21, 2, 376, DateTimeKind.Local).AddTicks(6321), "Katharina5@yahoo.com", "omvAWQLSKFRPqn7/zgQjaEP0AuJjqsB4NNjjFFhYCX8=", "nqm2O/paGuuFqHX5uXYDfXmRv8QR14tKaACmwJCr8j0=", new DateTime(2022, 1, 11, 15, 21, 2, 376, DateTimeKind.Local).AddTicks(6417), "Rafael_Pagac64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 11, 15, 21, 2, 393, DateTimeKind.Local).AddTicks(348), "hrr0eQHXdSDW9oIftRSwqYXGEGh8Q/pGd9JSyLQ+tKE=", "8dVT+bZw6cLX+CGc2z3YfwMVmJqFclL7o34048ew8ZY=", new DateTime(2022, 1, 11, 15, 21, 2, 393, DateTimeKind.Local).AddTicks(348) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(4717), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(5419), 15 },
                    { 18, 19, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6498), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6503), 8 },
                    { 17, 10, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6470), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6475), 7 },
                    { 15, 10, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6414), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6418), 17 },
                    { 14, 18, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6381), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6387), 5 },
                    { 13, 7, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6299), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6304), 8 },
                    { 12, 16, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6271), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6276), 21 },
                    { 11, 17, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6243), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6248), 1 },
                    { 19, 6, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6526), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6531), 16 },
                    { 10, 2, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6212), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6217), 8 },
                    { 8, 6, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6156), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6161), 9 },
                    { 7, 2, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6129), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6134), 9 },
                    { 6, 14, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6100), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6105), 8 },
                    { 5, 7, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6071), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6076), 3 },
                    { 4, 3, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6041), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6046), 9 },
                    { 3, 7, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6008), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6013), 21 },
                    { 2, 7, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(5959), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(5968), 11 },
                    { 9, 9, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6184), false, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6189), 9 },
                    { 20, 2, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6554), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6558), 19 },
                    { 16, 11, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6442), true, new DateTime(2021, 12, 31, 20, 43, 36, 857, DateTimeKind.Local).AddTicks(6447), 8 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Eaque nam consequatur illum et adipisci quidem voluptates voluptate ut.", new DateTime(2021, 12, 31, 20, 43, 36, 843, DateTimeKind.Local).AddTicks(9112), 1, new DateTime(2021, 12, 31, 20, 43, 36, 843, DateTimeKind.Local).AddTicks(9788) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Id quia dolorem.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(410), 3, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(421) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Perferendis temporibus ut et odit dicta.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(511), 13, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(517) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Ut corporis modi ea rerum esse ex et et provident.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(664), 11, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(671) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 21, "Nihil et distinctio magni.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(736), new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(742) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Autem qui impedit ut.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(867), 20, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(873) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Eligendi ab cumque qui.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(929), 12, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(935) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Consequatur odit quae distinctio iure et vel occaecati.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1005), 12, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1011) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Voluptas at commodi sint tenetur reiciendis fugit dignissimos dignissimos.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1086), 7, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1092) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Tenetur quam distinctio veniam nam animi ut.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1242), 10, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1250) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Molestias esse quia laborum totam qui eligendi dolor ad alias.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1336), 9, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1343) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Consequuntur assumenda est explicabo.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1399), 13, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1406) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Ratione totam eligendi excepturi explicabo.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1462), 15, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1468) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Quo sint ipsa est quis.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1525), 7, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1531) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Deserunt nisi molestias provident facere distinctio repudiandae ipsam laudantium omnis.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1675), 11, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1681) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Necessitatibus quas illum consectetur adipisci et eaque.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1796), 17, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1804) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Tempora eos ea et ex sapiente ea.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1876), 6, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1882) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Qui sit facere eius vitae unde atque iste autem.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1955), 11, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(1961) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Amet qui dolorem velit modi culpa quia explicabo corporis modi.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(2036), 17, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(2042) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Tempore qui mollitia iste aliquid.", new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(2100), 11, new DateTime(2021, 12, 31, 20, 43, 36, 844, DateTimeKind.Local).AddTicks(2106) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 555, DateTimeKind.Local).AddTicks(5898), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/934.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(1664) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2382), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/947.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2391) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2427), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1030.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2432) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2452), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/800.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2457) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2477), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/129.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2482) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2501), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/665.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2506) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2526), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1217.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2550), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/332.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2555) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2574), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1066.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2579) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2599), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/532.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2603) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2623), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1235.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2628) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2647), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/744.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2652) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2671), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/545.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2676) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2797), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/601.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2803) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2828), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/610.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2832) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2852), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/818.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2856) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2876), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/332.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2900), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/107.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2904) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2924), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/838.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2928) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2948), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1103.jpg", new DateTime(2021, 12, 31, 20, 43, 36, 556, DateTimeKind.Local).AddTicks(2952) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(3326), "https://picsum.photos/640/480/?image=217", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4066) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4356), "https://picsum.photos/640/480/?image=550", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4363) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4389), "https://picsum.photos/640/480/?image=447", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4394) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4415), "https://picsum.photos/640/480/?image=183", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4440), "https://picsum.photos/640/480/?image=513", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4445) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4525), "https://picsum.photos/640/480/?image=913", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4552), "https://picsum.photos/640/480/?image=911", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4557) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4578), "https://picsum.photos/640/480/?image=147", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4583) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4603), "https://picsum.photos/640/480/?image=255", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4608) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4628), "https://picsum.photos/640/480/?image=620", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4653), "https://picsum.photos/640/480/?image=958", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4658) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4678), "https://picsum.photos/640/480/?image=769", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4682) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4703), "https://picsum.photos/640/480/?image=687", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4707) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4727), "https://picsum.photos/640/480/?image=137", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4732) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4753), "https://picsum.photos/640/480/?image=168", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4758) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4777), "https://picsum.photos/640/480/?image=918", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4782) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4802), "https://picsum.photos/640/480/?image=883", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4807) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4827), "https://picsum.photos/640/480/?image=1059", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4832) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4894), "https://picsum.photos/640/480/?image=194", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4899) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4921), "https://picsum.photos/640/480/?image=146", new DateTime(2021, 12, 31, 20, 43, 36, 561, DateTimeKind.Local).AddTicks(4926) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7631), true, 17, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7636), 7 },
                    { 1, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(5756), true, 14, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(6445), 16 },
                    { 19, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7603), false, 6, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7608), 19 },
                    { 18, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7577), false, 7, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7581), 13 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7549), true, 8, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7554), 19 },
                    { 16, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7521), true, 1, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7526), 18 },
                    { 14, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7463), false, 11, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7467), 10 },
                    { 13, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7433), true, 14, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7438), 3 },
                    { 12, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7405), true, 9, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7410), 20 },
                    { 11, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7377), false, 19, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7382), 1 },
                    { 15, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7491), false, 18, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7496), 18 },
                    { 9, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7319), true, 19, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7323), 20 },
                    { 8, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7290), true, 12, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7295), 11 },
                    { 7, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7263), false, 18, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7267), 17 },
                    { 6, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7234), false, 19, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7239), 6 },
                    { 10, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7346), true, 18, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7350), 4 },
                    { 5, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7207), false, 14, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7211), 6 },
                    { 4, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7179), true, 16, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7183), 8 },
                    { 3, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7146), true, 7, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7151), 14 },
                    { 2, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7090), false, 12, new DateTime(2021, 12, 31, 20, 43, 36, 851, DateTimeKind.Local).AddTicks(7100), 16 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "nihil", new DateTime(2021, 12, 31, 20, 43, 36, 831, DateTimeKind.Local).AddTicks(7474), 30, new DateTime(2021, 12, 31, 20, 43, 36, 831, DateTimeKind.Local).AddTicks(8164) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Cumque sit ipsa.", new DateTime(2021, 12, 31, 20, 43, 36, 836, DateTimeKind.Local).AddTicks(4285), 40, new DateTime(2021, 12, 31, 20, 43, 36, 836, DateTimeKind.Local).AddTicks(4315) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Magnam dolor iste facere ratione neque perspiciatis. Quisquam ut suscipit pariatur veritatis. Aliquam ea sunt commodi sint enim modi dolor libero laudantium.", new DateTime(2021, 12, 31, 20, 43, 36, 836, DateTimeKind.Local).AddTicks(8605), 34, new DateTime(2021, 12, 31, 20, 43, 36, 836, DateTimeKind.Local).AddTicks(8632) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Similique saepe soluta accusantium sit sit odit tenetur.", new DateTime(2021, 12, 31, 20, 43, 36, 836, DateTimeKind.Local).AddTicks(8770), 27, new DateTime(2021, 12, 31, 20, 43, 36, 836, DateTimeKind.Local).AddTicks(8777) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Itaque maiores consequuntur excepturi natus hic voluptas molestiae quidem.\nEarum doloribus eum laboriosam rem excepturi explicabo sunt qui illum.\nPorro dolorem libero nemo fugiat aut eum corporis sunt.\nSoluta magnam dolores quis autem ut.\nAut rem qui voluptas non repellat quod aperiam amet nulla.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(1213), 33, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(1245) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "non", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(1390), 40, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(1395) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Praesentium sed qui quasi. Fuga perspiciatis nihil voluptatem quod sit ab soluta ut qui. Aliquam incidunt dicta temporibus eum molestiae enim facilis eaque adipisci.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(1647), 38, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(1655) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Sunt quasi harum vitae maxime autem. Est qui eum in voluptatum exercitationem. Autem tenetur rem architecto perspiciatis cumque dolorum quo molestiae. Provident recusandae vitae aperiam maxime. Tempore deleniti ut laboriosam possimus reiciendis quae rerum in.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2043), 33, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2052) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Magnam ducimus aut temporibus et labore.\nEt in voluptatem dolor id facilis.\nUnde vel nostrum quis ut.\nQuo explicabo itaque a aspernatur vel.\nQui cum recusandae pariatur aperiam necessitatibus voluptatem beatae dicta a.\nTemporibus quas quis accusamus vitae.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2391), 31, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2399) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "suscipit", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2449), 35, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2465) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Soluta error eligendi nesciunt excepturi sit.\nMolestiae suscipit aut consequuntur laudantium ipsum maxime pariatur.\nModi laudantium vero repellat culpa in aut.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2660), 37, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2667) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Beatae voluptatum neque et sunt et.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2779), 28, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2785) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Et sunt earum id ea.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2893), 24, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(2899) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Vel quia quo occaecati debitis.\nVoluptatem eveniet veniam iste at qui explicabo dolorum.\nHic ipsum natus aperiam nulla aut reprehenderit.\nExercitationem corporis necessitatibus sint reprehenderit modi tempora.\nExplicabo reprehenderit quasi qui ut omnis.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3168), 33, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3175) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Totam quasi est voluptatum omnis.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3238), 30, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3243) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Alias quo est perspiciatis nihil et quam eius doloribus vel. Cum illo odit possimus placeat voluptatem aperiam quo consectetur provident. Libero unde suscipit voluptatem. Illum cupiditate voluptate mollitia in vel incidunt dignissimos.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3483), 28, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3489) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Nisi nihil vel unde consequatur.\nSit nam porro accusantium praesentium quis minima eius totam.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3602), 37, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3608) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "non", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3649), 23, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3655) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Eius non et dolorem nam autem magni.\nDebitis praesentium nulla odit vel sed.\nPerspiciatis ullam dicta aut.\nRerum enim quia mollitia omnis quidem.\nQui voluptatem ab ut quod.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3879), 35, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3886) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Non adipisci repellat nisi saepe delectus voluptate qui quis.", new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3962), 28, new DateTime(2021, 12, 31, 20, 43, 36, 837, DateTimeKind.Local).AddTicks(3968) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 12, 31, 20, 43, 36, 607, DateTimeKind.Local).AddTicks(394), "Tessie_Bartoletti11@yahoo.com", "u9b7oEe/gYUii0x66L1Wgo+wpDv0HHsbxkkZjOJhbkM=", "Joz2DrmrWKykO60SHOCybXxYB3x2bM+bVx8guEKkzXg=", new DateTime(2021, 12, 31, 20, 43, 36, 607, DateTimeKind.Local).AddTicks(1211), "Jessy29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 12, 31, 20, 43, 36, 618, DateTimeKind.Local).AddTicks(1899), "Lewis59@hotmail.com", "bPY5NriLh3BBGvqVTyTZhgP1Xt3iUqvwl4uNzFRoVLM=", "JorNszaGiJzkLXpSMkxROs+lzSOA33Y5YPhcgqOTWYg=", new DateTime(2021, 12, 31, 20, 43, 36, 618, DateTimeKind.Local).AddTicks(1936), "Bertrand_Prosacco" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 12, 31, 20, 43, 36, 629, DateTimeKind.Local).AddTicks(8305), "Rudy77@yahoo.com", "7UfZ4kwzUV+aqq9DryfceSxSU2cu8gPAp0pm7XABmFU=", "illPRkVkEBSe6MdUFl9IJOa03FQ7umQLuPF4/YrDRJw=", new DateTime(2021, 12, 31, 20, 43, 36, 629, DateTimeKind.Local).AddTicks(8354), "Kennedi5" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 12, 31, 20, 43, 36, 644, DateTimeKind.Local).AddTicks(7209), "Jordy38@yahoo.com", "fmFtrq+0vAkuKsVfVAItjI2+wWXE0aeJph3X8F5wI8I=", "p0fq3I9g83lGLYtrakCcoabAzictJ8LvrtkMUPALlKo=", new DateTime(2021, 12, 31, 20, 43, 36, 644, DateTimeKind.Local).AddTicks(7250), "Aurelie_Reichert" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 12, 31, 20, 43, 36, 655, DateTimeKind.Local).AddTicks(2488), "Carey_Rippin@yahoo.com", "3uZdDMIalxRl9LvuWI0CN4fTrTBBQzUvvFm9ge8YDPw=", "oFJi3sG2A3KMGwWy7w4h2lcAIdBYb2Vr6Hd+VxE+1+k=", new DateTime(2021, 12, 31, 20, 43, 36, 655, DateTimeKind.Local).AddTicks(2521), "Maymie57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 12, 31, 20, 43, 36, 665, DateTimeKind.Local).AddTicks(8818), "Gonzalo0@yahoo.com", "jX26dexiTgSVucAdG/t8PTQZ6rZkKTneIrtLNhbxayw=", "yeG42GYKRYbXl8hMOMgmK4+IS0Kl0EB8y++weagtv9Y=", new DateTime(2021, 12, 31, 20, 43, 36, 665, DateTimeKind.Local).AddTicks(8867), "Dock.Swift" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 12, 31, 20, 43, 36, 676, DateTimeKind.Local).AddTicks(3984), "Bertrand.Grimes66@hotmail.com", "DG7dp+cZx5uVfJ0oOhdzI4fSp3RacA71Li/4yiGVksU=", "euKgwRwqSRj6wE612wx/YXJpnnl3fXnlvBXKmkf7i/o=", new DateTime(2021, 12, 31, 20, 43, 36, 676, DateTimeKind.Local).AddTicks(4002), "Monserrat_Buckridge" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 12, 31, 20, 43, 36, 686, DateTimeKind.Local).AddTicks(8635), "Caitlyn.Gulgowski31@gmail.com", "GEMqtmG0UOVhyik59C4fKdxKt7iL0GuswxeqhEPgflg=", "3JC35F8JA3oCs4OLlNRz8DwfFMcQxTe4CzzQf7dM2xw=", new DateTime(2021, 12, 31, 20, 43, 36, 686, DateTimeKind.Local).AddTicks(8648), "Kellen34" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 12, 31, 20, 43, 36, 697, DateTimeKind.Local).AddTicks(4511), "Kelli_Schamberger@gmail.com", "aEawkhHNLa1nYhQcgnzhKdwGJSh3egL240XnzzQmTFo=", "R8xixxZRnsb7VF4aa0KHGIKIgZgV+CoRAErEtjruXa4=", new DateTime(2021, 12, 31, 20, 43, 36, 697, DateTimeKind.Local).AddTicks(4537), "Ettie_Jaskolski58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 12, 31, 20, 43, 36, 708, DateTimeKind.Local).AddTicks(2270), "Alfonso.Borer13@gmail.com", "bAVbhMwryk1zK5B6M4F8vcLMh0akeRGeojA8iXkAWdA=", "q3VtqyQCueD3wF+v9Ym3aerkoIWTuYufMDMP8nEYuUc=", new DateTime(2021, 12, 31, 20, 43, 36, 708, DateTimeKind.Local).AddTicks(2297), "Jazlyn.Bartell17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 12, 31, 20, 43, 36, 718, DateTimeKind.Local).AddTicks(6965), "Sonia.Larkin@hotmail.com", "kk1QgRsGT9zuZ16WNlUueyr3kpG9dkGRPhl2b9KU1oQ=", "XU+n4ywGz3qmoUyymROD6w9oufp4IQUDEhsf8qHk664=", new DateTime(2021, 12, 31, 20, 43, 36, 718, DateTimeKind.Local).AddTicks(6978), "Rebeca57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 12, 31, 20, 43, 36, 729, DateTimeKind.Local).AddTicks(3300), "Arden.Goldner@hotmail.com", "BLXU4e7YjUI9Y75G/4EjaA9UjhBJHAI7eMLsB9fFxmY=", "MXF7TZbdpgyrM5/rPVvhaZ3eFbvinUzdysEV5zow7/o=", new DateTime(2021, 12, 31, 20, 43, 36, 729, DateTimeKind.Local).AddTicks(3350), "Kip99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 12, 31, 20, 43, 36, 739, DateTimeKind.Local).AddTicks(9120), "Rowena_Reichert@yahoo.com", "5HACf1Z/UBxk4dHfZsNEyFcTDAvjjwYvRelY8EbpJ8U=", "62/pkDjhfJY3LC+y0JAGTSW3VnWvAGM85Sncevn7K04=", new DateTime(2021, 12, 31, 20, 43, 36, 739, DateTimeKind.Local).AddTicks(9160), "Diana.Bode7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 750, DateTimeKind.Local).AddTicks(4181), "Jackeline19@hotmail.com", "9sdBX64aGLJo4FWGPVxImiNCkYXL78UNHc/fTJWVOjQ=", "ph50LeDt7m+IXwmZUCvgPXhrSBXGgGx9xwcU3PpPQ7U=", new DateTime(2021, 12, 31, 20, 43, 36, 750, DateTimeKind.Local).AddTicks(4198), "Dominic18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 12, 31, 20, 43, 36, 761, DateTimeKind.Local).AddTicks(120), "Alexie.Veum41@yahoo.com", "71NKdne1SZfainK2O9pd1YXtwnX+7kELqcEIrkr7rvM=", "jpnSHA4vzVUoD7/0SjLUR73i5rPNVefvy2TpoTE2/Cg=", new DateTime(2021, 12, 31, 20, 43, 36, 761, DateTimeKind.Local).AddTicks(153), "Mellie1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 12, 31, 20, 43, 36, 771, DateTimeKind.Local).AddTicks(5316), "Kraig84@gmail.com", "OT/geFIEGQ1daUm9uByDHTaslajSq3wfhFgxHicZ1sc=", "EEH3NlKeqY68NYF6Sr2DLpbvW2IQ/zp5dZ1IKpjRUps=", new DateTime(2021, 12, 31, 20, 43, 36, 771, DateTimeKind.Local).AddTicks(5349), "Estevan_Jakubowski88" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 12, 31, 20, 43, 36, 782, DateTimeKind.Local).AddTicks(590), "Daisha.Murphy38@hotmail.com", "/nOFfr6EsYuhKE/NQ72wpRArLsqIdIOOyuivRLUnhtk=", "d1HfHQmkDM2lJwnPa7l2cFbbXpKNvwhDTN5OQEY4jOo=", new DateTime(2021, 12, 31, 20, 43, 36, 782, DateTimeKind.Local).AddTicks(612), "Jane58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 12, 31, 20, 43, 36, 792, DateTimeKind.Local).AddTicks(6251), "Madelynn.Moen56@gmail.com", "Vcoaeu56B5OrHV+DCOifVPABOinXn9Me70MjTJpSQQg=", "c40wyDE/4RQh0k0Vga73sEUoFekjibJz1r3+UEEPFJ0=", new DateTime(2021, 12, 31, 20, 43, 36, 792, DateTimeKind.Local).AddTicks(6285), "Norval_MacGyver" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 12, 31, 20, 43, 36, 803, DateTimeKind.Local).AddTicks(2521), "Ellen37@gmail.com", "xJRRQR+z41x0K05kHicGQP4ziFs74ohLCzSLKsvvEbA=", "/GoW9GPJ6QZiYPSf3rQdz+REjI6lUNkyF/ftXZaAKCw=", new DateTime(2021, 12, 31, 20, 43, 36, 803, DateTimeKind.Local).AddTicks(2595), "Cleta73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 12, 31, 20, 43, 36, 813, DateTimeKind.Local).AddTicks(8324), "Jaeden_Dickinson44@gmail.com", "FG54AkZ1+Z19qpgmcdrn8HASERi3cGWv6dnUpGQPN8w=", "DkPg3vzeK3qurMQ997pCR/IQF2p9qstYCdKET4GXW2w=", new DateTime(2021, 12, 31, 20, 43, 36, 813, DateTimeKind.Local).AddTicks(8364), "Donnell.Wintheiser38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 12, 31, 20, 43, 36, 824, DateTimeKind.Local).AddTicks(2860), "u/O271jnkodB6c77Rwmjby8fIh0gndePopKJcZ5xCRg=", "mDRkceUzHa9ji+mlVN7AKNNY2Ob+9Wh0kUYZg40XqAY=", new DateTime(2021, 12, 31, 20, 43, 36, 824, DateTimeKind.Local).AddTicks(2860) });
        }
    }
}
