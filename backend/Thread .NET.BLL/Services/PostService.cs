﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }
        
        public async Task DeletePost(DeletePostDTO postDeleteDto)
        {
            var postEntity = await _context.Posts
                .FirstOrDefaultAsync(p => p.Id == postDeleteDto.Id);

            if (postEntity is null || postEntity.AuthorId != postDeleteDto.AuthorId)
            {
                throw new NotFoundException(nameof(Post), postDeleteDto.Id);
            }
            
            _context.Posts.Remove(postEntity);
            await _context.SaveChangesAsync();
        }
        
        public async Task UpdatePost(PostUpdateDTO postUpdateDto)
        {
            var postEntity = await _context.Posts
                .Include(p=>p.Preview)
                .FirstOrDefaultAsync(p => p.Id == postUpdateDto.Id);

            if (postEntity is null || postEntity.AuthorId != postUpdateDto.AuthorId)
            {
                throw new NotFoundException(nameof(Post), postUpdateDto.Id);
            }

            var timeNow = DateTime.Now;

            postEntity.Body = postUpdateDto.Body;
            postEntity.UpdatedAt = timeNow;
            
            if (!string.IsNullOrEmpty(postUpdateDto.PreviewImage))
            {
                if (postEntity.Preview == null)
                {
                    postEntity.Preview = new Image
                    {
                        URL = postUpdateDto.PreviewImage
                    };
                }
                else
                {
                    postEntity.Preview.URL = postUpdateDto.PreviewImage;
                    postEntity.UpdatedAt = timeNow;
                }
            }
            else
            {
                if (postEntity.Preview != null)
                {
                    _context.Images.Remove(postEntity.Preview);
                }
            }

            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();
        }
    }
}
