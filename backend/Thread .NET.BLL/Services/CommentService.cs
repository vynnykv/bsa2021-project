﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task UpdateComment(CommentUpdateDTO commentUpdateDto)
        {
            var commentEntity = await _context.Comments
                .FirstOrDefaultAsync(c => c.Id == commentUpdateDto.Id);

            if (commentEntity is null || commentEntity.AuthorId != commentUpdateDto.AuthorId)
            {
                throw new NotFoundException(nameof(Comment), commentUpdateDto.Id);
            }

            var timeNow = DateTime.Now;

            commentEntity.Body = commentUpdateDto.Body;
            commentEntity.UpdatedAt = timeNow;
            
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteComment(CommentDeleteDTO commentDeleteDto)
        {
            var commentEntity = await _context.Comments
                .FirstOrDefaultAsync(c => c.Id == commentDeleteDto.Id);
            
            if (commentEntity is null || commentEntity.AuthorId != commentDeleteDto.AuthorId)
            {
                throw new NotFoundException(nameof(Comment), commentDeleteDto.Id);
            }
            
            _context.Comments.Remove(commentEntity);
            await _context.SaveChangesAsync();
        }
    }
}
