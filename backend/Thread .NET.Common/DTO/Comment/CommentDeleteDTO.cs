﻿namespace Thread_.NET.Common.DTO.Comment
{
    public class CommentDeleteDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
    }
}