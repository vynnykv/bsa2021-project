﻿namespace Thread_.NET.Common.DTO.Comment
{
    public class CommentUpdateDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public string Body { get; set; }
    }
}