﻿namespace Thread_.NET.Common.DTO.Post
{
    public class DeletePostDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
    }
}